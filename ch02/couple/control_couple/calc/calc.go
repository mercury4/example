package calc

func Calc(a int, b int, flag int) int {
	var result int
	if flag == 1 {
		result = a + b
	} else if flag == 2 {
		result = a - b
	} else if flag == 3 {
		result = a * b
	} else if flag == 4 {
		result = a / b
	}

	return result
}
