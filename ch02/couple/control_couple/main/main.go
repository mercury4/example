package main

import (
	"fmt"

	"gitlab.com/mercury4/example/ch02/couple/control_couple/calc"
)

func main() {
	var a, b int = 10, 20
	fmt.Println("a+b:", calc.Calc(a, b, 1))
	fmt.Println("a-b:", calc.Calc(a, b, 2))
}
