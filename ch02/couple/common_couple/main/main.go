package main

import (
	"fmt"

	"gitlab.com/mercury4/example/ch02/couple/common_couple/add"
	"gitlab.com/mercury4/example/ch02/couple/common_couple/data"
)

func main() {
	var a, b int = 10, 20
	add.Add(a, b)
	fmt.Println("sum:", data.Sum)
}
