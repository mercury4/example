package main

import (
	"fmt"

	"gitlab.com/mercury4/example/ch02/couple/tag_couple/calc"
)

func main() {
	var a, b int = 10, 20
	var sum int
	calc.Add(&a, &b, &sum)
	fmt.Println("a+b:", sum)
}
