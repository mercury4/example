package calc

func Add(a *int, b *int, sum *int) {
	*sum = *a + *b
}

func Sub(a *int, b *int, sum *int) {
	*sum = *a - *b
}
