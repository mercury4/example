package main

import (
	"fmt"

	"gitlab.com/mercury4/example/ch02/couple/content_couple/add"
	"gitlab.com/mercury4/example/ch02/couple/content_couple/calc"
)

func main() {
	var a, b int = 10, 20
	add.Add(a, b)
	fmt.Println("sum:", add.Sum)

	a, b = 32, 33
	fmt.Println("sum:", calc.Add(a, b))
}
