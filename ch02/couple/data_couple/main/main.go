package main

import (
	"fmt"

	"gitlab.com/mercury4/example/ch02/couple/data_couple/calc"
)

func main() {
	var a, b int = 10, 20
	var sum int
	sum = calc.Add(a, b)
	fmt.Println("a+b:", sum)
}
