package main

import (
	"html/template"
	"log"
	"net/http"
	"strings"
)

var _ template.HTML
var searchTemplate = `
<html>
<script type="text/javascript" src="https://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
    function submit() {
        $.ajax({
            url: "/submit",
            data: {name: 'jenny'},
            type: "POST",
            //dataType: "json",
            success: function(data) {
				alert(data);
            }
        });
    }
</script>

	<body>
		<form method="POST" action="/search">
		<input name="keyword" type="text" value="{{.Keyword}}">
		<button>搜索</button>
		<div>您搜索的关键词是: {{.Keyword}}</div>
		</form>
	</body>
</html>
`

type SearchData struct {
	Keyword string
}

func main() {
	http.HandleFunc("/search", handle)
	http.HandleFunc("/submit", submit)
	log.Println("Starting http server ...")
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func handle(w http.ResponseWriter, r *http.Request) {

	data := &SearchData{}
	data.Keyword = r.FormValue("keyword")

	output := strings.ReplaceAll(searchTemplate, "{{.Keyword}}", data.Keyword)
	w.Write([]byte(output))

	/*
		t := template.New("search")
		t.Parse(searchTemplate)
		t.Execute(w, data)
	*/
}

func submit(w http.ResponseWriter, r *http.Request) {
	log.Printf("user submit data\n")
	w.Write([]byte("succ"))
}
